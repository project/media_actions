<?php


/**
 * Action info hook
 *
 * @return unknown
 */
function media_actions_action_info() {
  return array(
    'media_actions_bliptv_store_action' => array(
      'description' => t('Send media to blip.tv; keep it synced when node is edited or deleted.'),
      'type' => 'node',
      'configurable' => TRUE,
      'hooks' => array(
        'nodeapi' => array('insert', 'update', 'delete'),
        )
      )
  );
}


/**
 * Action form hook
 *
 * @param unknown_type $context: default values for the form
 * @return the form description
 */
function media_actions_bliptv_store_action_form($context) {
  // default values for form
  if (!isset($context['username'])) $context['username'] = '';
  if (!isset($context['password'])) $context['password'] = '';
  if (!isset($context['timeout'])) $context['timeout'] = '300';
  if (!isset($context['license'])) $context['license'] = '1';
  if (!isset($context['signature'])) $context['signature'] = '';
  if (!isset($context['srcfield'])) $context['srcfield'] = 'local_video';
  if (!isset($context['dstfield'])) $context['dstfield'] = 'remote_video';
  if (!isset($context['node_types'])) $context['node_types'] = array();
  
  $form = array();

  // add form components
  $form['username'] = array(
    '#type' => 'textfield',
    '#title' => t('blip.tv User Name'),
    '#description' => t("Name of the blip.tv user."),
    '#default_value' => $context['username'],
    '#size' => '20',
    '#maxlength' => '254',
    '#required' => TRUE,
  );
  $form['password'] = array(
    '#type' => 'textfield',
    '#title' => t('blip.tv Password'),
    '#default_value' => $context['password'],
    '#size' => '20',
    '#maxlength' => '254',
    '#description' => t('Password for the blip.tv user above.'),
    '#required' => TRUE,
  );
  $form['timeout'] = array(
    '#type' => 'textfield',
    '#title' => t('Upload Timeout'),
    '#default_value' => $context['timeout'],
    '#field_suffix' => t('seconds'),
    '#size' => '4',
    '#maxlength' => '4',
    '#description' => t('How long we should wait for the transfer from our server to blip.tv server to complete.'),
    '#required' => TRUE,
  );
  $license['-1'] = 'No license (All rights reserved)';
  $license['1'] = 'Creative Commons Attribution';
  $license['2'] = 'Creative Commons Attribution-NoDerivs';
  $license['3'] = 'Creative Commons Attribution-NonCommercial-NoDerivs';
  $license['4'] = 'Creative Commons Attribution-NonCommercial';
  $license['5'] = 'Creative Commons Attribution-NonCommercial-ShareAlike';
  $license['6'] = 'Creative Commons Attribution-ShareAlike';
  $license['7'] = 'Public Domain';
  $form['license'] =  array(
    '#type' => 'select',
    '#title' => t('Global License Type'),
    '#options' => $license,
    '#default_value' => $context['license'],
    '#description' => t('All video will be uploaded and attached with the specified license. Description of licenses here <a href="http://creativecommons.org/licenses/">www.creativecommons.org/licenses</a>'),
    '#required' => TRUE,
  );
  $form['signature'] = array(
    '#type' => 'textfield',
    '#title' => t('Global Signature'),
    '#length' => 5,
    '#default_value' => $context['signature'],
    '#description' => t('This text will be appended to any video description uploaded to your blip.tv account e.g. a link back to your site from blip.tv')
  );
  $form['srcfield'] = array(
    '#type' => 'textfield',
    '#title' => t('Local Video Field Name'),
    '#default_value' => $context['srcfield'],
    '#description' => t("Name of the node field containing the video file path."),
    '#required' => TRUE,
  );
  $form['dstfield'] = array(
    '#type' => 'textfield',
    '#title' => t('Remote Video Field Name'),
    '#default_value' => $context['dstfield'],
    '#description' => t("Name of the node field where the uploaded video URL will be stored."),
    '#required' => TRUE,
  );

  $types = node_get_types('names');
  $form['node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node types'),
    '#default_value' => $context['node_types'],
    '#options' => $types,
    '#description' => t('Action will be executed on the selected node types.'),
  );
  
  return $form;
}


/**
 * Action form validate hook
 *
 * @param unknown_type $form
 * @param unknown_type $form_values
 */
function media_actions_bliptv_store_action_validate($form, $form_values) {
  if (!is_numeric($form_values['timeout']) || ((int)$form_values['timeout'] < 0)) {
    form_set_error('timeout', t("The timeout must be a non-negative numeric value."));
  }
}


/**
 * Action form submit hook
 *
 * @param unknown_type $form
 * @param unknown_type $form_values
 * @return unknown
 */
function media_actions_bliptv_store_action_submit($form, $form_values) {
  $params = array(
    'username'  => $form_values['username'],
    'password'  => $form_values['password'],
    'timeout'   => $form_values['timeout'],
    'license'   => $form_values['license'],
    'signature' => $form_values['signature'],
    'srcfield'  => $form_values['srcfield'],
    'dstfield'  => $form_values['dstfield'],
    'node_types'=> $form_values['node_types'],
  );
  return $params;
}


/**
 * The action hook
 *
 * @param unknown_type $node
 * @param unknown_type $context
 */
function media_actions_bliptv_store_action(&$node, $context = array()) {
  if ($context['hook'] == 'nodeapi') {
    if ($context['op'] == 'insert') {  
      if (_media_actions_bliptv_check_params($node, $context) && _media_actions_bliptv_send($node, $context)) {
        watchdog('media_actions', t('Sent node id %id media to blip.tv', array('%id' => intval($node->nid))));
      }
    }
    else if ($context['op'] == 'update') {  
      if (_media_actions_bliptv_check_params($node, $context) && _media_actions_bliptv_update($node, $context)) {
        watchdog('media_actions', t('Updated node id %id media to blip.tv', array('%id' => intval($node->nid))));
      }
    }
    else if ($context['op'] == 'delete') {  
      if (_media_actions_bliptv_check_params($node, $context) && _media_actions_bliptv_delete($node, $context)) {
        watchdog('media_actions', t('Deleted node id %id media to blip.tv', array('%id' => intval($node->nid))));
      }
    }
  }
}

/**
 * Check all parameters are ok for te action to execute on the node.
 *
 * @param $node
 * @param $params
 * @return TRUE if all is ok
 */
function _media_actions_bliptv_check_params($node, $params) {
  // check node type
  $node_type = node_get_types('type', $node);
  if ($params['node_types'][$node_type->type] === 0) {
    // do not process this node type
    return FALSE;
  }
  
  // check user name
  if (!$params['username']) {
    $message = t("blip.tv username not set");
    watchdog('media_actions', $message, 'WATCHDOG_ERROR');
    return FALSE;
  }
  
  // check password
  if (!$params['password']) {
    $message = t("blip.tv password not set");
    watchdog('media_actions', $message, 'WATCHDOG_ERROR');
    return FALSE;
  }

  // check srcfield
  if ((!$params['srcfield'])) {
    $message = t("Local video field not set");
    watchdog('media_actions', $message, 'WATCHDOG_ERROR');
    return FALSE;
  }
  if (!array_key_exists($params['srcfield'], get_object_vars($node))) {
    $message = t("Node type !n does not have a field named !f.", array('!n' => $node_type, '!f' => $params['srcfield']));
    watchdog('media_actions', $message, 'WATCHDOG_ERROR');
    return FALSE;
  }

  // check dstfield
  if ((!$params['dstfield'])) {
    $message = t("Remote video field not set");
    watchdog('media_actions', $message, 'WATCHDOG_ERROR');
    return FALSE;
  }
  if (!array_key_exists($params['dstfield'], get_object_vars($node))) {
    $message = t("Node type !n does not have a field named !f.", array('!n' => $node_type, '!f' => $params['dstfield']));
    watchdog('media_actions', $message, 'WATCHDOG_ERROR');
    return FALSE;
  }
  
  return TRUE;  
}

/**
 * Sends file to bliptv.
 * @node Node containing file to send.
 * @params Action configuration parameters.
 * @blid_file_id If this is set, we will update the bliptv file instead of creating a new one.
 * @return FALSE on error, TRUE on success.
 *
 */
function _media_actions_bliptv_send($node, $params = array(), $blip_file_id = null) {
  $username = $params['username'];
  $password = $params['password'];
  $local_video_field_name = $params['srcfield'];
  $remote_video_field_name = $params['dstfield'];
   
  $local_video_field = &$node->$local_video_field_name;
  $local_video = array_shift($local_video_field);
  
  if ($local_video['flags']['delete']) {
    // video file is being deleted
    return _media_actions_bliptv_delete($node, $params);
  }
  
  $file_path = $local_video['filepath'];
  $file_name = $local_video['filename'];
  
  // Upload file to blip.tv
  $post_data['file'] = "@" . $file_path;
  $post_data['description'] = $file_name . $edit['signature']; // Any valid text or HTML
  $post_data['title'] = $node->title; // Maximum length 255. No HTML.
  $post_data['post'] = 1; // Required: must set this to 1
  $post_data['license'] = $edit['license']; // Defaults to "-1" or "No license"
  $post_data['categories_id'] = -1; // "-1" means no category. Never use a value of "0".
  $post_data['userlogin'] = $username; // valid username
  $post_data['password'] = $password; // valid password
  if (isset($blip_file_id)) {
      $post_data['id'] = $blip_file_id; // update the file
  }
  
  $response = array();

  $timeout = intval($edit['timeout']);
  set_time_limit($timeout);
  
  // @TODO use drupal_http_request()
  // for encoding data, see http://api-drupal.pajunas.com/api/function/_openid_encode_message/DRUPAL-5
  // and also: http://www.w3.org/TR/html4/interact/forms.html
  $curl_session = curl_init();
  curl_setopt($curl_session, CURLOPT_URL, "http://www.blip.tv/?section=file&cmd=post&skin=api");
  
  curl_setopt($curl_session, CURLOPT_POST, 1 );
  curl_setopt($curl_session, CURLOPT_POSTFIELDS, $post_data);
  curl_setopt($curl_session, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($curl_session, CURLOPT_TIMEOUT, $timeout);
  $response['data'] = curl_exec($curl_session);
  
  if (curl_errno($curl_session)) {
    $response['error'] = curl_error($curl_session); 
  }
  curl_close($curl_session);
  
  if ($response['error']) {
    $message = "CURL error: ".$response['error'] . " for file " . $post_data['file'];    
    watchdog('media_actions', $message, 'WATCHDOG_ERROR');
    return FALSE; 
  }
  
  $data = trim($response['data']);
  $vals = $index = $array = array();
  $parser = xml_parser_create();
  xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
  xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
  $xml_parse_result = xml_parse_into_struct($parser, $data, $vals, $index);

  if ($xml_parse_result == 0) {
    $file_id = 0;
    $file_url = "";
    $message = t("Error in blip.tv answer: ") . xml_error_string(xml_get_error_code($parser)) . t(" at line ") . xml_get_current_line_number($parser);
    watchdog('media_actions', $message, 'WATCHDOG_ERROR');
  }
  else
  {
    $file_id = $vals[$index['item_id'][0]]['value'];
    $file_url = $vals[$index['link'][0]]['attributes']['href'];
  }
  
  xml_parser_free($parser);
  
  if ($file_id != 0) {
    $remote_video_field = &$node->$remote_video_field_name;
    if ($file_id != $remote_video_field[0]['value']) {
      $remote_video_field[0]['embed'] = $file_url;
      $remote_video_field[0]['value'] = $file_id;
      $remote_video_field[0]['provider'] = 'bliptv';
      node_validate($node);
    }
      
    $local_video['flags']['delete'] = TRUE;
    
    return TRUE;
  }
  else {
    return FALSE;
  }
}


/**
 * Update file on bliptv.
 * @node Node containing file to update.
 * @params Action configuration parameters.
 * @return FALSE on error, TRUE on success.
 *
 */
function _media_actions_bliptv_update($node, $params = array()) {
  $remote_video_field_name = $params['dstfield'];
  $remote_video_field = &$node->$remote_video_field_name;
  $blip_id = $remote_video_field[0]['value'];
  
  _media_actions_bliptv_send($node, $params, $blip_id);
}

/**
 * Delete file from bliptv.
 * @node Node containing file to delete.
 * @params Action configuration parameters.
 * @return FALSE on error, TRUE on success.
 *
 */
function _media_actions_bliptv_delete($node, $params = array()) {  
  $username = $params['username'];
  $password = $params['password'];
  $remote_video_field_name = $params['dstfield'];
  
  $remote_video_field = $node->$remote_video_field_name;  
  $bliptv_file_id = $remote_video_field[0]['value'];
    
  $reason = urlencode(t("Source node deleted"));
  $username = urlencode($username);
  $password = urlencode($password);
  $response = drupal_http_request("http://www.blip.tv/?section=file&cmd=delete&id=$bliptv_file_id&reason=$reason&userlogin=$username&password=$password&skin=api");
  
  if (isset($response->error)) {
    watchdog('media_actions', $response->error, 'WATCHDOG_ERROR');    
    return FALSE; 
  }
  
  $data = trim($response->data);
  $vals = $index = $array = array();
  $parser = xml_parser_create();
  xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
  xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
  $xml_parse_result = xml_parse_into_struct($parser, $data, $vals, $index);

  if ($xml_parse_result == 0) {
    $message = t("Error in blip.tv answer: ") . xml_error_string(xml_get_error_code($parser)) . t(" at line ") . xml_get_current_line_number($parser);
    watchdog('media_actions', $message, 'WATCHDOG_ERROR');
  }
  else
  {
    if (!$vals[$index['deleted'][0]]['value']){
      $message = t("Could not delete blip.tv file");
      watchdog('media_actions', $message, 'WATCHDOG_ERROR');
    }
  }
  
  xml_parser_free($parser);
  
  $remote_video_field[0]['embed'] = '';
  $remote_video_field[0]['value'] = '';
  $remote_video_field[0]['provider'] = '';
  node_validate($node);
  
  return TRUE;
}
