********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Media Actions Module
Author: Patrick Fournier <patrick at whiskyechobravo dot com>
Drupal: 6
********************************************************************
PREREQUISITES:
* Trigger module (included in core)
* Video Field (provided by Media Field module)
* Embedded Video Field (provided by Embedded Media Field module)

DESCRIPTION:

This module defines actions related to media files management 
for the Drupal core action function. Actions provided by this module
currently handle video storage on blip.tv; you are welcome to add
similar actions to this module.

********************************************************************
INSTALLATION:

1. Place the entire media_actions directory into your Drupal modules
   directory (normally sites/all/modules).

2. Enable the media_action module by navigating to:

     Administer > Site building > Modules

3. If you want anyone besides the administrative user to be able
   to configure actions (usually a bad idea), they must be given
   the "administer actions" access permission:
   
     Administer > User management > Permissions

   When the module is enabled and the user has the "administer
   actions" permission, an "actions" menu should appear under 
   Administer > Site configuration in the menu system.
   
********************************************************************
USE:
1. Create a CCK content type with two fields: 
  a. A Video Field (provided by the CCK mediafield module),
     used for video uploads.
  b. An Embedded Video Field (provided by Embedded Media Field 
     module), used to display the embedded blip.tv video.

2. Configure the Media Actions at 
   Administer > Site configuration > Actions.
  a. Select the action(s) under "Make a new advanced action 
     available" andclick Create.
  b. Fill in the required information.
     - Enter your Video Field (upload) name in the Local Video Field
     Name section.
     - Enter your Embedded Video Field (display) name in the Remote 
     Video Field Name section.

When creating a node, the user should enter a filename in the Video 
Field. The file will be uploaded as usual to your Drupal server, then 
the media action will upload this file to blip.tv, delete the local 
file and modify the Embedded Video Field to point to the newly created 
blip.tv video.
